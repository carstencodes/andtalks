#!/bin/bash

. venv/bin/activate

# Use build-in local creation script
invoke createReveal

# Archive slides and reveal.js sources
tar -zcf slides.tgz slides.html reveal.js

# Add images to slides archive, if there are any
(test -d images && tar -zrf slides.tgz images) || echo "No images to tag"
