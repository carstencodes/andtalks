#!/bin/bash

# Get python version
python -V; 

# Enable virtual environments
pip install virtualenv;

# Create and activate a virtual python environemtn
virtualenv venv; 
source venv/bin/activate

# Add python pandoc controller 
pip install pypandoc;
# Add invoker
pip install invoke;
# Add docker wrapper
pip install docker;

# install pandoc tar gzip and python git accessing library
apt-get update && apt-get install pandoc python3-pygit2 tar gzip -y;

# Override installed pandoc version by externally developed version, but keep installed dependencies
wget https://github.com/jgm/pandoc/releases/download/2.7.2/pandoc-2.7.2-1-amd64.deb -O pandoc.deb && dpkg -i pandoc.deb && rm -f pandoc.deb;
