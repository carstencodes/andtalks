#!/bin/bash

# smoothen commit ref name (branch name) - CAVEAT: branch can contain forward slashes
export CI_COMMIT_REF_NAME_SMOOTHED=${CI_COMMIT_REF_NAME//\//_}
# remove dashes
export CI_COMMIT_REF_NAME_SMOOTHED=${CI_COMMIT_REF_NAME_SMOOTHED//-/_}
# remove dots
export CI_COMMIT_REF_NAME_SMOOTHED=${CI_COMMIT_REF_NAME_SMOOTHED//./_}

export CI_COMMIT_MESSAGE_VARIABLE_NAME=$!CI_RELEASE_MESSAGE_${CI_COMMIT_REF_NAME_SMOOTHED}

export CI_COMMIT_DESCRIPTION=${!CI_COMMIT_MESSAGE_VARIABLE_NAME}

echo Checking variable $CI_COMMIT_MESSAGE_VARIABLE_NAME for content ...
if [ -z $CI_COMMIT_DESCRIPTION ]
then
    echo No commit description set. Aborting release creation ...
    exit 1
fi

echo done

CI_COMMIT_DESCRIPTION="$CI_COMMIT_DESCRIPTION"

echo Using the following commit description for the release and the scm tag: $CI_COMMIT_DESCRIPTION

# Create a unique file name identifier from branch and pipeline ID 
export CI_UNIQUE_FILE_NAME=${CI_COMMIT_REF_NAME_SMOOTHED}_$CI_PIPELINE_IID

# Create unique files - using quotes due to mv busybox script
mv -v slides.pdf slides-$CI_UNIQUE_FILE_NAME.pdf
mv -v slides.tgz slides-$CI_UNIQUE_FILE_NAME.tgz

# Use branch name for commit tag
CI_TAG_NAME=${CI_COMMIT_REF_NAME//\// }
CI_COMMIT_TAG_CREATED=

# Capitalize tags
for token in $CI_TAG_NAME;
do
    token_capitalized="$(tr '[:lower:]' '[:upper:]' <<< ${token:0:1})${token:1}"
    CI_COMMIT_TAG_CREATED=$CI_COMMIT_TAG_CREATED\ $token_capitalized
done

# Set capitalized tag
export CI_COMMIT_TAG=ReleaseSlidesForTalk${CI_COMMIT_TAG_CREATED// /}

echo Using tag $CI_COMMIT_TAG for to tag release

# Create tag on repository
curl -X POST --silent --show-error --fail "${CI_API_V4_URL}/${CI_PROJECT_ID}/repository/tags?tag_name=${CI_COMMIT_TAG}&ref=${CI_COMMIT_SHA}&private_token=${GITLAB_PAT}"

# Create access token from PAT variable
export GITLAB_ACCESS_TOKEN=$GITLAB_PAT

# Run gitlab release attaching the artifact files 
./gitlab-release --debug --message="$CI_COMMIT_DESCRIPTION" $CI_PROJECT_DIR/slides-$CI_UNIQUE_FILE_NAME.pdf $CI_PROJECT_DIR/slides-$CI_UNIQUE_FILE_NAME.tgz

# EOF