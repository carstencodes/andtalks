#!/bin/ash

# Install bash for execute script
apk add bash 

# Install curl for creating the git tag
apk add curl

# Add requests library for python
pip install requests
# download git-lab release
wget https://raw.githubusercontent.com/inetprocess/gitlab-release/master/gitlab-release -O gitlab-release && chmod a+x gitlab-release 