#!/bin/ash

SCRIPT_NAME=`basename $0`
SCRIPT_LOCATION=`dirname $0`

usage()
{
    echo Usage:
    echo $SCRIPT_NAME stage phase
    echo
    echo Arguments:
    echo \\t - stage:
    echo \\t\\t        The current CI stage
    echo \\t - phase:
    echo \\t\\t        The current script phase

    exit
}

test_arguments()
{
    if [ $# -lt 2 ]
    then
        usage
    fi; 

    if [ $# -gt 2 ]
    then
        usage 
    fi; 
}

main()
{
    test_arguments $@

    CURRENT_STAGE=$1
    CURRENT_PHASE=$2

    TARGET_SCRIPT_FILE=$SCRIPT_LOCATION/$CURRENT_STAGE/$CURRENT_PHASE.ash

    if [ ! -f $TARGET_SCRIPT_FILE ]
    then  
        echo "No file to execute for phase $CURRENT_PHASE in stage $CURRENT_STAGE. Expected was a script a $TARGET_SCRIPT_FILE."
        exit 1
    fi

    if [ ! -x $TARGET_SCRIPT_FILE ]
    then
        echo "Target script $TARGET_SCRIPT_FILE not executable. Aborting"
        exit 2
    fi

    echo Executing script for phase $CURRENT_PHASE in stage $CURRENT_STAGE
    . $TARGET_SCRIPT_FILE
    exit $?
}

main $@