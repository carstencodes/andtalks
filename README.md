# Talks held by Carsten I.

This repository contains all the talks I hold as stale branches. To view a
certain talk, just select the specific release file. Each talk held is
stored on a seperate branch. This will contain all slides created for the talk
and reference all sample files.

## Preperation

The following software must be installed on the local machine:

* `pandoc`, at least in version 2.7.2
* `docker`
* `python` 3 coming with the following packages
  * `docker`
  * `pygit2`
  * `pypandoc`
  * `invoke`
  * (for more details see the [pip requirements file](requirements.txt))

Depending on your system, the installation can be simplified using
`pip install -r requirements.txt` or `pip3 install -r requirements.txt`.

The build and presentation can be configured.

Within the [tasking file](tasks.py), some docker images might be required to use.
These are:

* `nginx:1-alpine` for the presentation unsing a local webserver
* `astefanutti/decktape` for the creation of PDF files.

A PDF file viewer and a webbrowser, which is up to date, must be installed as well.

## Configuration

The configuration parameters can be adjusted in the [configuration file](invoke.yml).
These are:

* `inputFile`: The markdown file containing slide sources that shall be converted using pandoc
* `outputFile`: The HTML file containing the reveal.js based presentation.
* `pdfOutputFile`: The PDF file to create from the reveal.js slides.
* `theme`: The reveal.js theme to use. (New themes can easily be created using sass)
* `revealJsUrl`: The location on the file system, where reveal.js is contained.
    * **NOTE**: This repository contains an up to date revision of the reveal.js sources
* `defaultPresentationMode`: The presentation mode to use. Valid values are:
    * `file`: Presentation runs using a `file:///` based URL. Not supported by some browsers
    * `local`: A local webserver is started using docker. The port can be configured below
    * `web`: The slides are hosted on a remote web host. The URL can be configured below 
* `serverPort`: The server port to use for the local webserver
* `remotePresentationHost`: The remote url excluding `outputFile` hosting the server

## Build

The presentation is created using python invoke. To create the slides, run

```shell
invoke createReveal
```

To create PDF files, just run

```shell
invoke createPdf
```

## Presenting

The presentation is created in the specified `outputFile` within the source 
directory. This file can be opened in any state of the art webbrowser. The 
invoke file can also be used for this:

```shell
invoke present
```

To specify the kind of presentation, an argument can be applied. This can
be `file`, `local` and `web`. If no value is applied, the configured
`defaultPresentationMode` will be used.

## License

The content and the samples are published under CC-BY 4.0. Unless stated otherwise, the
content is license by Carsten Igel.

### Acknowledgements

This presentation uses the following software:

* [pandoc](https://pandoc.org) - available under GPL.
* [reveal.js](https://revealjs.com/) - available under MIT license.
* [decktape](https://github.com/astefanutti/decktape) - available under MIT license.
* [gitlab](https://www.gitlab.com) - available under MIT license
* [gitlab-release](https://github.com/inetprocess/gitlab-release) - available under MIT license.
* [python](https://www.python.org/) - available under python software foundation license.
* [python invoke](https://www.pyinvoke.org/) - available under BSD-2 CLAUSE license.
* [docker](https://www.docker.com/) - available under Apache 2.0 license.
