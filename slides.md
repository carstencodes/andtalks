---
author: Carsten Igel
title: // Set Title here //
date: // Set date here //
---

# Who am I

* Carsten Igel
    * Software architect
    * Part time nerd
    * Linux enthusiast
* Programming and developing
    * C# / .NET (Full Framework, Core)
    * Python
* Focussed on
    * Cross platform development
    * Build tool chains
* Hobbies
    * Go (aka Weiqi, Baduk)
    * CCC
    * Honorary civilian protection
    * Raspberry PI

# What will I talk about today

* Topic 1
* Topic 2
    * Topic 2.1
    * Topic 2.2
* Topic 3

# Let's get it on

...

# Conclusion

...

# Contact

Questions? Remarks? Updates? Need help?

Contact me via:

* `carstencodes` on [github](https://github.com/carstencodes),
[gitlab](https://gitlab.com/carstencodes)
and [gitter](https://gitter.im/carstencodes)
* hubzilla: carstencodes@hub.disroot.org
* web: [http://www.carstenigel.net](http://www.carstenigel.net)

Slides on[gitlab/carstencodes/andtalks](https://gitlab.com/carstencodes/andtalks)

# Acknowledgements

This talk and the production of this slides was made possible by the usage of

* [pandoc](https://pandoc.org)
* [markdown](https://daringfireball.net/projects/markdown/)
* [reveal.js](https://revealjs.com/)
* [decktape](https://github.com/astefanutti/decktape)
* [gitlab](https://www.gitlab.com)
* [gitlab-release](https://github.com/inetprocess/gitlab-release)
* [python](https://www.python.org/)
* [python invoke](https://www.pyinvoke.org/)
* [docker](https://www.docker.com/)

## Literature

## Images

## Code snippets
