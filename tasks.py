# Default imports
from invoke import task
from invoke.tasks import call

from enum import Enum

from abc import ABC, abstractmethod

import os
import sys

##################
# Helping data structures (enum)

## Enum: Presentation mode

class DocumentedEnum(Enum):
    """
        Basic class for documentation enumerations using tuples as values and documentation string.
    """
    def __new__(cls, value, doc=None):
        """
            Creates a new instance of the enumeration class.

            Parameters:
            -----------
              cls: class
                 The class to instantiate
              value: 
                 The value to assign
              doc: str
                 The string to use as documentation
        """
        # Create a new instance using the specified value
        self = object.__new__(cls) 
        self._value_ = value
        # Set documentation, if second tuple value is set
        if doc is not None:
            self.__doc__ = doc

        # Return created value
        return self

class PresentationMode(Enum):
    """
        Mode of presentation to use.
    """
    Default = 'default', "The configuration value shall be taken."
    File = 'file', "A file based URL protocol shall be used."
    LocalHost = 'local', "A localhost webserver shall be used."
    RemoteHost = 'web', "Use a presentation hosted on remote webserver."

############
# Errors to occur 

# Error: Settings incomplete

class IncompleteSettingError(Exception):
    """
        Error that is used, when a setting has not been configured.
    """
    def __init__(self, parameter):
        """
            Creates a new instance of the IncompleteSettingError.

            Parameters:
            -----------
                parameter: str
                    The name of the parameter which is not properly configured.
        """
        super(IncompleteSettingError, self).__init__("The following setting has not been set via command-line, invoke configuration file (./invoke.yaml, invoke.yml, invoke.json, invoke.py) or environment variable INVOKE_<NAME>: %s" % parameter)

# Error: Incomplete implementation

class IncompleteImplementationError(Exception):
    """
        Error that is used, when a certain implementation is not present. The name of the function invoked is read from the stack.
    """
    def __init__(self):
        """
            Creates a new instance of the IncompleteImplementationError.
        """
        functionName = sys._getframe(1).f_code.co_name
        super(IncompleteImplementationError, self).__init__("The following method has not been fully implemented and hence, cannot be executed: %s" % functionName )


##################
# Service classes and factories

## Class: Url Builder service class and specializations

class UrlBuilder(ABC):
    """ 
        An URL build is designed to create a URL for a certain file.
    """
    @abstractmethod
    def getUrl(self, file):
        """
            Generates an URL for the specific file.

            Parameters:
            -----------
                file : str
                    The file to address via the result URL.

            Returns
            --------
            str
                The URL to the file.
        """
        return ""

class FileUrlBuilder(UrlBuilder):
    """
        This URL builder will generate URLs to address the file via the file:// protocol
    """
    def getUrl(self, file): ## INHERIT DOCUMENTATION
        import pathlib
        import os

        filePath = os.path.abspath(file)

        return pathlib.Path(filePath).as_uri()

class LocalHostUrlBuilder(UrlBuilder):
    """
        This URL builder will generate URLs for a webserver residing on the localhost using the http:// protocol
    """
    def __init__(self, serverPort):
        """
            Creates a new localhost based URL builder using the specified port.

            Parameters:
            -----------
                serverPort : int
                    The port of the webserver on the local host
        """
        self.__serverPort = int(serverPort)

    def getUrl(self, file): ## INHERIT DOCUMENTATION
        return "http://localhost:%i/%s" % (self.__serverPort, file)

class RemoteHostUrlBuilder(UrlBuilder):
    """
        This URL builder will generate URLs for a remote host webserver using the protocol of the basic remote server URL
    """
    def __init__(self, remoteHost):
        """
            Creates a new remote host base URL builder using the specified basic URL.

            Parameters:
            -----------
                remoteHost : str 
                    The basic URL of the remote host 
        """
        self.__remoteHost = str(remoteHost)

    def getUrl(self, file): ## INHERIT DOCUMENTATION
        return "%s/%s" % (self.__remoteHost, file)

## Factory: URL Builder

def createUrlBuilder(mode, config):
    """
        Factory method to create a URL builder using the specified mode and the PyInvoke configuration

        Parameters:
        -----------
            mode : PresentationMode 
                The presentation mode to address
            config : invoke.config.Config
                The configuration of the current context used by PyInvoke  
        
        Returns:
        --------
            UrlBuilder
                The URL builder to use.
    """
    if (PresentationMode.Default == mode):
        mode = PresentationMode[config['defaultPresentationMode']]

    if (PresentationMode.File == mode):
        return FileUrlBuilder()
    if (PresentationMode.LocalHost == mode):
        return LocalHostUrlBuilder(config['serverPort'])
    if (PresentationMode.RemoteHost == mode):
        return RemoteHostUrlBuilder(config['remotePresentationHost'])

## Class: Presentation preparer and specializations

class PresentationPreparer(ABC):
    """
        A presentation preparer will perform any steps necessary to ensure, that a presentation can take place
    """
    @abstractmethod
    def execute(self):
        """
            Call this method to ensure that the presentation of the specified mode can take place.
        """
        raise IncompleteImplementationError()

class DefaultPresentationPreparer(PresentationPreparer):
    """
        A presentation preparer doing nothing at all
    """
    def execute(self): ## INHERITED DOCUMENTATION
        pass

class LocalHostPresentationPreparer(PresentationPreparer):
    """
        A presentation preparer ensuring that the presentation can start using the localhost webserver
    """
    def execute(self): ## INHERITED DOCUMENTATION
        call(startHost)

## Factory: Presentation preparer

def createPresentationPreparer(mode, config):
    """
        Factory method to create a presentation preparer using the specified mode and the PyInvoke configuration

        Parameters:
        -----------
            mode : PresentationMode 
                The presentation mode to address
            config : invoke.config.Config
                The configuration of the current context used by PyInvoke   

        Returns:
        --------
            PresentationPreparer
                The presentation preparer to use.
    """
    if (PresentationMode.Default == mode):
        mode = PresentationMode[config['defaultPresentationMode']]
    
    if (PresentationMode.LocalHost == mode):
        return LocalHostPresentationPreparer()
    
    return DefaultPresentationPreparer()

###########
# Command classes

class Command(ABC):
    """
        Basic implementation of a command to execute.
    """
    @abstractmethod
    def run(self, c):
        """
            Executes the current command.

            Parameters:
            -----------
                c : invoke.context.Context
                    The invocation context used by PyInvoke.

            Returns:
            --------
                None
        """
        raise IncompleteImplementationError()

class CommandLineExecutable(Command):
    """
        The basic implementation for commands executed via command line.
    """
    def run(self, c): ## INHERITED DOCUMENTATION
        commandLine = self.__getCommandLine()

        c.run(commandLine)

    def sudo(self, c):
        """
            Will run the command line command with administrative rights.

            Parameters:
            -----------
                c : invoke.context.Context
                    The invocation context used by PyInvoke.

            Returns:
            --------
                None
        """
        commandLine = self.__getCommandLine()

        c.sudo(commandLine)

    def _getCommandLineArguments(self):
        """
            Creates the command line arguments for the command to execute.

            Returns:
            --------
                str
                    The command line arguments.
        """
        return ""

    @abstractmethod
    def _getExecutableCommandName(self):
        """
            Returns the path to the command line executable to execute.

            Returns:
            --------
                str
                    The path to the executable to run.
        """
        raise IncompleteImplementationError()

    def __getCommandLine(self):
        """
            Uses the command name and the command line arguments to create the command to execute.

            Returns:
            --------
                str
                    The command line to execute
        """
        executable = self._getExecutableCommandName()
        arguments = self._getCommandLineArguments()

        commandLine = executable
        if arguments:
            commandLine = "%s %s" % (commandLine, arguments)

        return commandLine

class PanDoc(Command):
    """
        Basic command for pandoc based conversions.

        Properties:
        -----------
            InputFile: str
                The path to the file to convert.
            OutputFile: str
                The path to the file that shall represent the output of the conversion.
    """
    def run(self, c): ## INHERITED DOCUMENTATION
        import pypandoc

        target = self._getTargetFormat()
        source = self._getSourceFormat()
        extraArgs = self._getExtraArguments()

        output = pypandoc.convert_file(self.InputFile, to=target, format=source, outputfile=self.OutputFile, extra_args = extraArgs)

        print (output)

    def __get_InputFile(self):
        """
            Returns the name of the configured input file.

            Returns:
            --------
                str
                    The path to the input file.
        """
        return self.__inputFile

    def __set_InputFile(self, value):
        """
            Sets the name of the input file to convert.

            Parameters:
            -----------
                value: str
                    The path of the file to convert.
            
            Returns:
            --------
                None
        """
        self.__inputFile = value

    InputFile = property(__get_InputFile, __set_InputFile, doc = "The path of the file to convert.")

    def __get_OutputFile(self):
        """
            Gets the path to the output file.

            Returns:
            --------
                str
                    The path to the output file.
        """
        return self.__outputFile

    def __set_OutputFile(self, value):
        """
            Sets the path to the output file.

            Parameters:
            -----------
                value: str
                    The path of the output file to write to.

            Returns:
            --------
                None
        """
        self.__outputFile = value

    OutputFile = property(__get_OutputFile, __set_OutputFile, doc = "The path of the file that shall contain the converted output.")

    @abstractmethod
    def _getTargetFormat(self):
        """
            Gets the target format to convert to.

            Returns:
            --------
                str
                    The target format to convert to.
        """
        raise IncompleteImplementationError()

    @abstractmethod
    def _getSourceFormat(self):
        """
            Gets the target format to convert from.

            Returns:
            --------
                str
                    The target format to convert from.
        """
        raise IncompleteImplementationError()

    def _getExtraArguments(self):
        """
            Gets the extra arguments to apply to the conversion.

            Returns:
            --------
                Sequence of str
                    The arguments to apply to the pandoc command. One entry per argument.
        """
        return []

class Docker(Command):
    """
        Basic implementation for all docker based commands.
    """
    def __init__(self):
        """
            Initializes the docker command.
        """
        import docker
        print ("Allocating docker client ... ")
        self.__client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        print ("    done")

    def __get_Client(self):
        """
            Returns the current docker client.

            Returns:
            --------
                docker.DockerClient
                    The current docker clients.
        """
        return self.__client
    
    _client = property(__get_Client, doc = "The docker client used to control docker.")

    def _getTagName(self):
        """
            Gets the name of the tag to use when searching for image names.

            Returns:
            --------
                str
                    The tag to fetch.
        """
        return "latest"

    @abstractmethod
    def _getImageName(self):
        """
            Gets the name of the image to instantiate a container from.

            Returns:
            --------
                str
                    The name and repository of the image to use. 
        """
        raise IncompleteImplementationError()

    def _getVolumes(self):
        """
            Gets the volumes to mount to the container.
            The result is a dictionary in the form

                str -> dict

            where the string contains the local path and the dictionary contains further parameters like mode ('ro', 'rw') and 
            the bind location, i.e. the path within the container,
            e.g.:
                '/hello/world' : {'bind' : '/foo/bar', 'mode': 'rw' }

            Returns:
            --------
                dict
                    The dictionary of volumes to use.
        """
        return {}

    def _getPorts(self):
        """
            Gets the ports to bind. 
            The result is a dictionary of ports in the form 
            
                str -> int

            where the string consists of the port and the protocol separated by a slash, 
            e.g.:
                '80/tcp' -> 9000

            Returns:
            --------
                dict
                    The dictionary of ports to use.
        """
        return {}

    def _getImage(self):
        """
            Searches for the image to use for the resulting container. 
            If the image is not present, it will be pulled.

            Returns:
            --------
                docker.Image
                    The image found.
        """
        realRepositoryImageName = self.__getRepositoryImageName()

        print ("Searching for image '%s'..." % realRepositoryImageName)

        imageList = self.__client.images.list()
        for image in imageList:
            if (realRepositoryImageName in image.attrs['RepoTags']):
                print ("Found image. Using image %s ..." % image.id)
                return image

        return self.__pullImage()

    def _getImageId(self):
        """
            Searches for the internal id of the image to use.

            Returns:
            --------
                str
                    The id of the image
                None
                    The image has not been found.
        """
        realRepositoryImageName = self.__getRepositoryImageName()
        print ("Searching for image '%s'..." % realRepositoryImageName)

        imageList = self.__client.images.list()
        for image in imageList:
            if (realRepositoryImageName in image.attrs['RepoTags']):
                return image.id

        return None

    def __getRepositoryImageName(self):
        """
            Gets the full name of the image including repository and tag

            Returns:
            --------
                str
                    The name of the image in the repository
        """
        return "%s:%s" % (self._getImageName(), self._getTagName())
        
    def __pullImage(self):
        """
            Pulls the image from the remote docker registry.

            Returns:
            --------
                docker.Image
                    The image instance pulled
        """
        print ("Image not found. Pulling image ...")
        imageName = self.__getRepositoryImageName()
        return self.__client.images.pull(imageName)

    def _printContainerLogs(self, container):
        """
            Prints the logs from the container to the stdout and stderr streams.

            Parameters:
            -----------
                container: docker.Container
                    The container to use.

            Returns:
            --------
                None
        """
        import sys

        stdOut = container.logs(stdout =  True, stderr = False, stream = False, timestamps = False, tail = 'all', follow = False).decode('UTF-8')
        stdErr = container.logs(stdout =  False, stderr = True, stream = False, timestamps = False, tail = 'all', follow = False).decode('UTF-8')

        for line in (Docker.__splitNewLinesFromString(stdOut)):
            print (line, file=sys.stdout)

        for line in (Docker.__splitNewLinesFromString(stdErr)):
            print (line, file=sys.stderr)

    @staticmethod
    def __splitNewLinesFromString(str):
        """
            Splits the new lines from the strings.

            Parameters:
            -----------
                str: str
                    The string to split by new lines

            Returns:
            --------
                Sequence of str
                    The string split into multiple lines.
        """
        import re
        return re.split('\r|\n', str)

class StartDockerContainer(Docker):
    """
        Basic implementation of an asynchronous docker based execution step, i.e. the equivalent of 'docker start' and 'docker stop'.
        The container will be started using the run method of the command.
    """
    def run(self, c): ## INHERITED DOCUMENTATION

        container = self.__findRunningContainer(False)

        if (container is not None):
            print ("A container is already running. Aborting ...")
            return

        imageName = self._getImageName()
        ports = self._getPorts()
        volumes = self._getVolumes()

        print ("Searching docker container from image named '%s' " % imageName)

        image = self._getImage()

        print ("Starting docker container ...")

        self._client.containers.run(image, volumes= volumes, ports= ports, detach= True)

        print ("Container created ...")

    def stopContainer(self, c):
        """
            Stops the container belonging to this command.

            Parameters:
            -----------
                c : invoke.context.Context
                    The execution context of the PyInvoke environment
        """
        container = self.__findRunningContainer(False)

        if (container is None):
            print ("No container running")
            return

        print ("Found container %s (%s)" % (container.name, container.short_id))
        self._printContainerLogs(container)

        print ("Stopping docker container ...")
        container.stop()

    def __findRunningContainer(self, verbose):
        """
            Attempts to find a container belonging to the current image.

            Returns:
            --------
                docker.Container
                    The container for this command

                None
                    If no container was found.
        """
        imageName = self._getImageName()
        
        imageId = self._getImageId()

        if imageId is None:
            if verbose:
                print ("No image found locally. Aborting")
            return None

        print ("Searching for container based upon %s" % imageName)

        for container in self._client.containers.list():
            if (container.attrs['Config']['Image'] == imageId):
                return container

        return None

class RunInDocker(Docker):
    """
        Basic implementation of a synchronous command that will be executed in a docker container, i.e. the equivalent of 'docker run'.
    """
    
    def run(self, c): ## INHERITED DOCUMENTATION
        imageName = self._getImageName()
        command = self._getCommandToExecute()
        volumes = self._getVolumes()

        print ("Searching docker container from image named '%s' " % imageName)

        image = self._getImage()

        print ("Starting docker container ...")

        container = self._client.containers.run(image, command, volumes= volumes, detach=True )

        print ("Container created ...")

        print ("Waiting for container to be finished ...")

        container.wait()

        self._printContainerLogs(container)

    @abstractmethod
    def _getCommandToExecute(self):
        """
            Gets the command including the command line arguments to run in the docker.

            Returns:
            --------
                str
                    The command to execute in the docker.
        """
        raise IncompleteImplementationError()

########################
# Concrete command implementations
    
class RevealJsToPdfConversion(RunInDocker):
    """
        A docker based command converting reveal.js based slides to PDF files.
    """
    def __get_InputFile(self):
        """
            Gets the path to the HTML file to convert to PDF.

            Returns:
            --------
                str
                    The path to the HTML file.
        """
        return self.__inputFile

    def __set_InputFile(self, value):
        """
            Sets the name of the HTML file to convert to PDF.

            Parameters:
            -----------
                value : str
                    The path to the HTML file to convert.

            Returns:
            --------
                None
        """
        self.__inputFile = value

    InputFile = property(__get_InputFile, __set_InputFile, doc = "The path of the HTML file to convert to PDF")

    def __get_OutputFile(self):
        """
            Gets the path of the PDF file to write.

            Returns:
            --------
                str
                    The path to the resulting PDF document.
        """
        return self.__outputFile

    def __set_OutputFile(self, value):
        """
            Set the name of the PDF file to write.

            Parameters:
            -----------
                value : str
                    The path to the resulting PDF file.
            
            Returns:
            --------
                None
        """
        self.__outputFile = value

    OutputFile = property(__get_OutputFile, __set_OutputFile, doc = "The path to the resulting PDF file containing the converted reveal.js slides")

    def _getImageName(self): ## INHERITED DOCUMENTATION
        return "astefanutti/decktape" 

    def _getVolumes(self): ## INHERITED DOCUMENTATION
        import os
        curDir = os.getcwd()
        return { curDir : { 'bind': '/slides', 'mode': 'rw' }} 

    def _getCommandToExecute(self): ## INHERITED DOCUMENTATION
        return "reveal --size=1920x1080 /slides/%s /slides/%s" % (self.InputFile, self.OutputFile)

class MarkDownToRevealJsConversion(PanDoc):
    """
        PanDoc based command to convert a markdown file to an HTML file containing reveal.js based slides.
    """
    def __get_Theme(self):
        """
            Gets the name of the reveal.js the to use.

            Returns:
            --------
                str
                    The name of the theme to use.
        """
        return self.__theme
    
    def __set_Theme(self, theme):
        """
            Sets the name of the reveal.js theme to use.

            Parameters:
            -----------
                value : str
                    The name of the reveal.js theme to use.

            Returns:
            --------
                None
        """
        self.__theme = theme

    Theme = property(__get_Theme, __set_Theme, doc = "Sets the reveal.js theme to use.")

    def __get_RevealJsUrl(self):
        """
            Gets the path to the reveal.js installation.

            Returns:
            --------
                str
                    The path of the reveal.js installation
        """
        return self.__revealJsUrl

    def __set_RevealJsUrl(self, url):
        """
            Sets the url of the reveal.js URL

            Parameters:
            -----------
                url : str
                    The reveal.js URL to use
        """
        self.__revealJsUrl = url

    RevealJsUrl = property(__get_RevealJsUrl, __set_RevealJsUrl, doc = "Sets the URL to the local reveal.js files to include")

    def _getTargetFormat(self): ## INHERITED DOCUMENTATION
        return 'revealjs'

    def _getSourceFormat(self): ## INHERITED DOCUMENTATION
        return 'markdown'

    def _getExtraArguments(self): ## INHERITED DOCUMENTATION
        arguments = [ '-V', 'revealjs-url=%s' % self.RevealJsUrl, '-V', 'theme=%s' % self.Theme, '-V', 'center=false', '--slide-level=2', '--standalone' ]
        return arguments

class Presenter(Command):
    """
        Command that will open a webbrowser window and present the reveal.js slides
    """
    def __get_url(self):
        """
            Gets the URL of the reveal.js slides.

            Returns:
            --------
                str
                    URL of the reveal.js slides
        """
        return self.__url

    def __set_url(self, url):
        """
            Set the URL of the reveal.js slides.

            Parameters:
            -----------
                url : str
                    The URL of the reveal.js slides to present.
            
            Returns:
            --------
                None
        """
        if (not url):
            raise ValueError("No URL set")
        self.__url = url

    Url = property(__get_url, __set_url, doc = "Gets or sets the URL of the reveal.js slides to present")

    def run(self, c): ## INHERITED DOCUMENTATION
        import webbrowser

        webbrowser.open(self.Url)

class DockerBasedNginXWebServer(StartDockerContainer):
    """
        Docker based command to start a local webserver.
    """
    def __set_serverPort(self, port):
        """
            Sets the port of the webserver to use.

            Raises:
            -------
                ValueError
                    If the port is below 1024 or above 65535
            
            Parameters:
            -----------
                port : int
                    The port to use

            Returns:
            --------
                None
        """
        if (port < 1024):
            raise ValueError("Port must be greater than or equal to 1024")
        if (port > 65535):
            raise ValueError("Port must be less than or equal to 65535")
        
        self.__port = port

    def __get_serverPort(self):
        """
            Gets the port of the webserver to use.

            Returns:
            --------
                int
                    The port to use.
        """
        return self.__port

    ServerPort = property(__get_serverPort, __set_serverPort, doc = "The port of the webserver to use.")

    def __set_WebRoot(self, webRoot):
        """
            Sets the webroot of the local webserver.

            Raises:
            -------
                NotADirectoryError
                    The specified webRoot does not exist.
            
            Parameters:
            -----------
                webRoot : str
                    The location of the local webroot

            Returns:
            --------
                None
        """
        import os

        if (not os.path.isdir(webRoot)):
            raise NotADirectoryError()

        self.__webRoot = webRoot

    def __get_WebRoot(self):
        """
            Gets the web root of the local webserver.

            Returns:
            --------
                str
                    The webroot to use.
        """
        return self.__webRoot

    WebRoot = property(__get_WebRoot, __set_WebRoot, doc = "The location of the webserver local webroot.")

    def _getPorts(self): ## INHERITED DOCUMENTATION
        return { "80/tcp": self.ServerPort }

    def _getVolumes(self): ## INHERITED DOCUMENTATION
        import os
        curDir = os.getcwd() 
        return { curDir: { "bind" : "/usr/share/nginx/html", "mode": "ro" } }

    def _getImageName(self): ## INHERITED DOCUMENTATION
        return "nginx"

    def _getTagName(self): ## INHERITED DOCUMENTATION
        return "1-alpine"

################
# Helper function

# function: delete file, if present

def delete_file_if_found(f):
    """
        Tries to delete a file, if it can be found on the file system

        Parameters:
        -----------
            f : str
                Path to the file to delete

        Returns:
        --------
            None
    """ 
    if (os.path.isfile(f)):
        print ("Removing file %s ..." % f)
        os.remove(f)

################
# Tasks

@task
def clean(c):
    """
        Removes the output files, if present

        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context

        Returns:
        --------
            None
    """

    # Check configured values

    if (not c.config.outputFile):
        delete_file_if_found(c.config.outputFile)

    if (c.config.pdfOutputFile):
        delete_file_if_found(c.config.pdfOutputFile)
    

@task
def createReveal(c):
    """
        Creates the reveal.js based slides using (py-)pandoc
                
        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context

        Returns:
        --------
            None
    """
    print ("Creating Reveal.js slides")
    md2rjs = MarkDownToRevealJsConversion()

    if (not c.config.inputFile):
        raise IncompleteSettingError("inputFile")

    if (not c.config.outputFile):
        raise IncompleteSettingError("outputFile")

    if (not c.config.theme):
        raise IncompleteSettingError("theme")

    if (not c.config.revealJsUrl):
        raise IncompleteSettingError("revealJsUrl")
    
    md2rjs.InputFile = c.config.inputFile
    md2rjs.OutputFile = c.config.outputFile
    md2rjs.Theme = c.config.theme
    md2rjs.RevealJsUrl = c.config.revealJsUrl

    md2rjs.run(c)

@task(pre=[createReveal], help={'kind': "The source url kind to start the presentation with. (See documentation for details)"})
def present(c, kind='default'):
    """
        Opens the default webbrowser and starts a presentation with reveal.js slides

        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context
            kind : PresentationMode
                The presentation mode to use.

        Returns:
        --------
            None
    """
    p = Presenter()

    if (not c.config.outputFile):
        raise IncompleteSettingError("outputFile")

    mode = PresentationMode[kind]

    presentationPreparer = createPresentationPreparer(mode, c.config)
    presentationPreparer.execute()

    urlbuilder = createUrlBuilder(mode, c.config)

    p.Url = urlbuilder.getUrl(c.config.outputFile)

    p.run(c)

@task()
def startHost(c):
    """
        Starts an nginx based webserver using docker

        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context

        Returns:
        --------
            None
    """
    h = DockerBasedNginXWebServer()

    if (not c.config.serverPort):
        raise IncompleteSettingError("serverPort")

    h.ServerPort = c.config.serverPort

    h.run(c)

@task()
def stopHost(c):
    """
        Stops the nginx based webserver using docker

        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context

        Returns:
        --------
            None
    """ 
    h = DockerBasedNginXWebServer()
    h.stopContainer(c)

@task(pre=[createReveal])
def createPdf(c):
    """
        Creates a PDF file from the reveal.js slides

        Parameters:
        -----------
            c : invoke.context.Context
                PyInvoke Context

        Returns:
        --------
            None
    """
    r = RevealJsToPdfConversion()

    if (not c.config.pdfOutputFile):
        raise IncompleteSettingError("pdfOutputFile")
    
    r.InputFile = c.config.outputFile
    r.OutputFile = c.config.pdfOutputFile

    print ("Creating PDF file '%s' ..." % c.config.pdfOutputFile)
    r.run(c)

# EOF
